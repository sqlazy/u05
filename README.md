# u05-api-cicd

Group assignment u05-api-cicd  

An implementation of an API in Python with unit tests, documentation and ci/cd pipelines.  

## API  
This API is constructed with FastAPI and Psycopg2.  
To connect to our database you will need to add an environment file ```.env``` containing environment variables for the database connection.     

### Endpoints  
- **GET /stores**  
  Returns name and full address of all stores in db  
- **GET /stores/{name}**  
  Returns information of one specific store by specifying its name  
  Output is formatted by query parameter ```res_format```:  
  - full (default): returns name and full address  
  - name: returns only name  
  - compact: returns name and city  
- **GET /cities**  
  Returns all unique cities where a store is located.  
  It accepts the following query parameter:  
  - ```zipcode``` filter cities by zipcode. Can be given more than once.  
- **GET /sales**  
  Returns a list of all transactions  
- **GET /sales/{sale-id}**  
  Returns information of a specific transaction by its UUID  
- **GET /income**  
  Returns data from transactions, including price and discount percent.  
  It accepts the following query parameters:  
  - ```store``` (can be given more than once) UUID to filter results by store  
  - ```product``` (can be given more than once) UUID to filter results by
    product  
  - ```from``` filter out all transactions before the given datestamp/timestamp  
  - ```to``` filter out all transactions after the given datestamp/timestamp  
- **GET /inventory**  
  Returns data about *current* inventory (inventory - sales) showing inventory status per product and store.  
  It accepts the following query parameters:  
  - ```store``` UUID to filter results by store  
  - ```product``` UUID to filter results by product  

---  

## Installation  
- First of all, create and activate a virtual environment with the tool that fits your personality and masochistic tendencies.  
- Install required libraries with pip install -r requirements.txt  

## Usage  
Run Uvicorn from project root: ```uvicorn app.main:app```  
The API is now running on localhost where you can try it with Swagger UI.  

# For development  
Create and activate a virtual environment.  
Install required libraries with ```pip install -r requirements_dev.txt```  
Run Uvicorn from project root: ```Uvicorn app.main:app --reload```  
Tests are written for Pytest and uses a custom Italian artisanal database mocker

---  

# GitLab CI/CD  
YAML file for GitLab CI/CD can be found in project root: ```.gitlab-ci.yml```  
It consists of three stages: code quality, tests and build and it uses two separate pipelines:  
- Stage code quality when pushing to a branch without an open merge request  
- Stages tests and build when opening a merge request  

---

---  

# Docker Setup
To run the API inside a docker container, simply use: 
- ```sudo docker run -dt --name sqlazy_api -p 80:80 registry.gitlab.com/sqlazy/u05:latest```

This will launch a container and run the API inside of it. To interact with the files within the container, use:

- ```sudo docker exec -it sqlazy_api bash```

---

## Authors and acknowledgment  

Abdullah Wasuqe  
Carl Mikaelsson  
Daniel Goldman Lapington  
Erik Olsson  
Tomas Karlsson  
Viktor Berg  

With well needed guidance by the almighty Giaco.  

---  

## License  
This is free and unencumbered software released into the public domain.  

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.  

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.  

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.  

For more information, please refer to <https://unlicense.org>  
