"""API backend for web shop"""
from collections import namedtuple
from enum import Enum
from os import environ
from typing import List, Optional
from uuid import UUID

from dotenv import load_dotenv

from fastapi import FastAPI, HTTPException, Query

import psycopg

load_dotenv()

app = FastAPI()


@app.on_event("startup")
def startup():
    """Open database connection on startup"""
    app.db = psycopg.connect(  # pragma: no cover
        f"""postgresql://{
        environ['DB_USER']}:{environ['DB_PASSWORD']}@{environ['DB_HOST']}/{environ['DB']
        }"""
    )
    app.db.execute("SET client_encoding TO UNICODE")  # pragma: no cover


@app.on_event("shutdown")
def shutdown():
    """Close database connection on shutdown"""
    app.db.close()  # pragma: no cover


@app.get("/")
def root() -> dict:
    """
    Endpoint for root
    :return: A small welcome message
    """
    return {"greeting": "Hello visitor!"}


@app.get("/stores")
def stores() -> dict:
    """
    Endpoint to get name and full address of all stores in database
    :return: dict
    """
    with app.db.cursor() as cur:
        cur.execute(
            """
            SELECT stores.name, store_addresses.city, store_addresses.address, store_addresses.zip
            FROM stores
            FULL OUTER JOIN store_addresses
            ON stores.id=store_addresses.store
            """)
        db_response = cur.fetchall()
    data = []
    for store in db_response:
        name, city, street, zip_code = store
        address = f"{street}, {zip_code} {city}"
        data.append({"name": name, "address": address})
    return {"data": data}


@app.get("/cities")
def cities(zipcode: Optional[List[str]] = Query(None, max_length=5)) -> dict:
    """Endpoint to get all unique cities or get city from zip code.
    Zip code can be given more than once.

    :param zipcode: List[str] : Optional zip code(s) to get city from
    :return: All unique cities where there is a store.
             If query parameter zipcode is given it filters by zip code
    """
    if zipcode:
        with app.db.cursor() as cur:
            cur.execute(
                """
                SELECT city
                FROM store_addresses
                WHERE zip=ANY(%s);
                """, (zipcode,))
            db_response = cur.fetchall()
        if not db_response:
            raise HTTPException(status_code=404, detail="404 Not Found")
        # I suppose that city can't be null in db.
        # So city[0] will always contain name of a city
        return {"data": [city[0] for city in db_response]}
    with app.db.cursor() as cur:
        cur.execute("SELECT distinct city FROM store_addresses;")
        city = cur.fetchall()
    # I suppose that city can't be null in db.
    # So city_list[0] will always contain name of a city.
    return {"data": [city_list[0] for city_list in city]}


class StoreFormat(str, Enum):
    """Enum class to define available query parameters for GET /stores/{name}?res_format"""
    FULL = "full"
    NAME = "name"
    COMPACT = "compact"


@app.get("/stores/{name}")
def stores_name(name: str, res_format: Optional[StoreFormat] = "full") -> dict:
    """
    Endpoint to get the full address of a store chosen by name
    :param res_format: StoreFormat : format for output. Can be full, name or compact
    :param name: str : Name to search for in database
    :return: dict
    """
    with app.db.cursor() as cur:
        cur.execute(
            """
            SELECT stores.name, store_addresses.city, store_addresses.address, store_addresses.zip
            FROM stores
            FULL OUTER JOIN store_addresses
            ON stores.id=store_addresses.store WHERE name = %s;
            """, (name.title(),))
        db_response = cur.fetchall()
    if not db_response:
        raise HTTPException(status_code=404, detail="404 Not Found")
    name, city, street, zip_code = db_response[0]
    if res_format == "name":
        return {"data": [{"name": name}]}
    if res_format == "compact":
        return {"data": [{"name": name, "city": city}]}
    return {"data": [{"name": name, "address": f'{street}, {zip_code} {city}'}]}


@app.get("/sales")
def sales() -> dict:
    """
    Endpoint to get sales
    :return: dict : store, time, sales_id
    """
    with app.db.cursor() as cur:
        cur.execute(
            """
            SELECT stores.name, sales.time, sales.id
            FROM sales
            LEFT JOIN stores ON sales.store=stores.id
            """
        )
        db_response = cur.fetchall()
    data = []
    for sale in db_response:
        name, date_time, sale_id = sale
        date_time = str(date_time).replace(" ", "T").replace("-", "")
        data.append({"store": name, "timestamp": date_time, "sale_id": str(sale_id)})
    return {"data": data}


@app.get("/sales/{sale_id}")
def sales_id(sale_id: UUID) -> dict:
    """
    Endpoint to get sales on sales_id
    :param sale_id : UUID
    Data validation is properly managed by Pydantic.
    :return: dict : store, time, sales_id, products
    """
    with app.db.cursor() as cur:
        cur.execute(
            """
                SELECT stores.name, sales.time, sales.id, products.name, sold_products.quantity
                FROM stores
                JOIN sales ON stores.id = sales.store
                JOIN sold_products ON sold_products.sale = sales.id
                JOIN products ON products.id = sold_products.product
                WHERE sales.id = %s;
                """, (sale_id,))
        db_response = cur.fetchall()
    if not db_response:
        raise HTTPException(status_code=404, detail="404 Not Found")
    name, date_time, db_id = db_response[0][:3]
    date_time = str(date_time).replace(" ", "T").replace("-", "")
    products = [{"name": sale[-2], "qty": sale[-1]} for sale in db_response]
    data = {
        "store": name,
        "timestamp": str(date_time),
        "sale_id": str(db_id),
        "products": products
    }
    return {"data": data}


QueryResultIncome = namedtuple("QueryResultIncome",
                               ("store_name", "product_name", "price",
                                "quantity", "sale_time", "discount"))


@app.get("/income")
def get_income(store: Optional[List[str]] = Query(None),
               product: Optional[List[str]] = Query(None),
               from_=Query(None, alias="from"), to_=Query(None, alias="to")) -> dict:
    """GET /income

    Returns data in the usual format {"data": ·list-of-dicts}. Each
    dictionary contains all info about a transaction, including price and
    discount percent.

    It accepts the following query parameters:
        - store: (can be given more than once) UUID to filter results by store
        - product: (can be given more than once) UUID to filter results by
          product
        - from: filter out all transactions before the given datestamp/timestamp
        - to: filter out all transactions after the given datestamp/timestamp

    If any invalid UUID is given (either in store or product), 422 -
    Unprocessable Entity will be returned
    """
    stores_clause, products_clause, from_clause, to_clause = "", "", "", ""
    parameters = []
    if store:
        try:
            for iterator in store:
                UUID(iterator)
        except ValueError as err:
            raise HTTPException(status_code=422,
                                detail="Invalid UUID given for store!") from err
        stores_clause = "WHERE stores.id = ANY(%s)"
        parameters.append(store)
    if product:
        try:
            for iterator in product:
                UUID(iterator)
        except ValueError as err:
            raise HTTPException(
                status_code=422,
                detail="Invalid UUID given for product!") from err
        products_clause = "WHERE products.id = ANY(%s)"
        if parameters:
            products_clause = products_clause.replace("WHERE", "AND")
        parameters.append(product)
    if from_:
        from_clause = "WHERE sales.time >= %s"
        if parameters:
            from_clause = from_clause.replace("WHERE", "AND")
        parameters.append(from_)
    if to_:
        to_clause = "WHERE sales.time <= %s"
        if parameters:
            to_clause = to_clause.replace("WHERE", "AND")
        parameters.append(to_)
    query = """SELECT stores.name, products.name, prices.price,
               sold_products.quantity, sales.time, discounts.discount_percent
               FROM sold_products
               JOIN products on sold_products.product = products.id
               JOIN sales ON sold_products.sale = sales.id
               JOIN stores ON sales.store = stores.id
               JOIN prices ON products.id = prices.product
               LEFT JOIN discounts ON products.id = discounts.product
               {stores} {products} {from_} {to}
               ORDER BY sales.time;"""
    query = query.format(stores=stores_clause, products=products_clause,
                         from_=from_clause, to=to_clause)
    try:
        with app.db.cursor() as cur:
            cur.execute(query, parameters)
            result = cur.fetchall()
    except psycopg.errors.Error as err:
        app.db.rollback()
        raise HTTPException(status_code=422, detail="Invalid datetime format!") from err
    entries = [QueryResultIncome(*r)._asdict() for r in result]
    return {"data": entries}


QueryResultInventory = namedtuple("QueryResultInventory", ("product_name",
                                                           "adjusted_quantity",
                                                           "store_name"))


@app.get("/inventory")
def get_inventory(store: Optional[str] = Query(None), product: Optional[str] = Query(None)) -> List:
    """GET /inventory

    Returns data in the usual format {"data": ·list-of-dicts}. Each
    dictionary contains all info about *current* inventory (inventory -
    sales) showing inventory status per product and store.

    It accepts the following query parameters:
        - store: UUID to filter results by store
        - product: UUID to filter results by product

    If any invalid UUID is given (either in store or product), 422 -
    Unprocessable Entity will be returned

    """
    store_clause, product_clause = "", ""
    parameters = []
    if store:
        try:
            UUID(store)
        except ValueError as err:
            raise HTTPException(status_code=422,
                                detail="Invalid UUID for store!") from err
        store_clause = "WHERE stores.id = %s"
        parameters.append(store)
    if product:
        try:
            UUID(product)
        except ValueError as err:
            raise HTTPException(status_code=422,
                                detail="Invalid UUID for product!") from err
        product_clause = "WHERE products.id = %s"
        if parameters:
            product_clause = product_clause.replace("WHERE", "AND")
        parameters.append(product)
    query = """SELECT products.name,
               SUM(inventory.quantity) - SUM(sold_products.quantity),
               stores.name
               FROM inventory
               JOIN products ON products.id = inventory.product
               JOIN stores ON stores.id = inventory.store
               JOIN sales ON sales.store = stores.id
               JOIN sold_products ON sold_products.product = products.id AND sold_products.sale = sales.id
               {store} {product}
               GROUP BY stores.name, products.name;
    """
    query = query.format(store=store_clause, product=product_clause)
    with app.db.cursor() as cur:
        cur.execute(query, parameters)
        result = cur.fetchall()
    entries = [QueryResultInventory(*r)._asdict() for r in result]
    return sorted(entries, key=lambda x: (x["store_name"], x["product_name"]))
