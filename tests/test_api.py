"""Unit tests for API"""
import datetime
from types import SimpleNamespace
from uuid import UUID

from fastapi.testclient import TestClient

from u05.app.main import app


all_inventories = [
    [
        "Hundmat",
        27,
        "Den Lilla Djurbutiken"
    ],
    [
        "Kattmat",
        170,
        "Den Lilla Djurbutiken"
    ],
    [
        "Hundmat",
        140,
        "Den Stora Djurbutiken"
    ],
    [
        "Kattklonare",
        68,
        "Den Stora Djurbutiken"
    ],
    [
        "Kattmat",
        345,
        "Den Stora Djurbutiken"
    ],
    [
        "Sömnpiller och energidryck för djur",
        61,
        "Den Stora Djurbutiken"
    ],
    [
        "Elefantkoppel",
        27,
        "Djuristen"
    ],
]

return_data = [
  {
    "product_name": "Hundmat",
    "adjusted_quantity": 27,
    "store_name": "Den Lilla Djurbutiken"
  },
  {
    "product_name": "Kattmat",
    "adjusted_quantity": 170,
    "store_name": "Den Lilla Djurbutiken"
  },
  {
    "product_name": "Hundmat",
    "adjusted_quantity": 140,
    "store_name": "Den Stora Djurbutiken"
  },
  {
    "product_name": "Kattklonare",
    "adjusted_quantity": 68,
    "store_name": "Den Stora Djurbutiken"
  },
  {
    "product_name": "Kattmat",
    "adjusted_quantity": 345,
    "store_name": "Den Stora Djurbutiken"
  },
  {
    "product_name": "Sömnpiller och energidryck för djur",
    "adjusted_quantity": 61,
    "store_name": "Den Stora Djurbutiken"
  },
  {
    "product_name": "Elefantkoppel",
    "adjusted_quantity": 27,
    "store_name": "Djuristen"
  }
]

all_incomes = [
    [
        "Den Stora Djurbutiken",
        "Sömnpiller och energidryck för djur",
        9.95,
        12,
        "2022-01-25T13:52:34",
        9
    ],
    [
        "Den Stora Djurbutiken",
        "Hundmat",
        109,
        3,
        "2022-01-25T13:52:34",
        "null"
    ],
    [
        "Djuristen",
        "Elefantkoppel",
        459,
        1,
        "2022-01-26T15:24:45",
        13
    ],
    [
        "Den Lilla Djurbutiken",
        "Kattmat",
        109,
        57,
        "2022-02-07T09:00:56",
        "null"
    ],
    [
        "Den Stora Djurbutiken",
        "Kattklonare",
        55900,
        1,
        "2022-02-27T12:32:46",
        25
    ],
    [
        "Den Stora Djurbutiken",
        "Kattmat",
        109,
        10,
        "2022-02-27T12:32:46",
        "null"
    ]
  ]

return_data_income = {"data": [
    {
        "store_name": "Den Stora Djurbutiken",
        "product_name": "Sömnpiller och energidryck för djur",
        "price": 9.95,
        "quantity": 12,
        "sale_time": "2022-01-25T13:52:34",
        "discount": 9
    },
    {
        "store_name": "Den Stora Djurbutiken",
        "product_name": "Hundmat",
        "price": 109,
        "quantity": 3,
        "sale_time": "2022-01-25T13:52:34",
        "discount": "null"
    },
    {
        "store_name": "Djuristen",
        "product_name": "Elefantkoppel",
        "price": 459,
        "quantity": 1,
        "sale_time": "2022-01-26T15:24:45",
        "discount": 13
    },
    {
        "store_name": "Den Lilla Djurbutiken",
        "product_name": "Kattmat",
        "price": 109,
        "quantity": 57,
        "sale_time": "2022-02-07T09:00:56",
        "discount": "null"
    },
    {
        "store_name": "Den Stora Djurbutiken",
        "product_name": "Kattklonare",
        "price": 55900,
        "quantity": 1,
        "sale_time": "2022-02-27T12:32:46",
        "discount": 25
    },
    {
        "store_name": "Den Stora Djurbutiken",
        "product_name": "Kattmat",
        "price": 109,
        "quantity": 10,
        "sale_time": "2022-02-27T12:32:46",
        "discount": "null"
    }
  ]
}


def db_mock(data):
    """This function returns a database mocking object, that will be used
    instead of the actual db connection.
    """
    database = SimpleNamespace()
    database.cursor = CursorMock(data)
    return database


class CursorMock:
    """This class mocks a db cursor. It does not build upon unittest.mock but
    it is instead built from an empty class, patching manually all needed
    methods.
    """
    def __init__(self, data):
        self.data = data

    def __enter__(self):
        return self

    def __exit__(self, *args):
        pass

    def __call__(self):
        return self

    @staticmethod
    def execute(*args):
        """This mocks cursor.execute. It returns args even though the return
        value of cursor.execute() is never used. This is to avoid the
        following linting error:

        W0613: Unused argument 'args' (unused-argument)
        """
        return args

    def fetchall(self):
        """This mocks cursor.fetchall.
        """
        return self.data


def test_root_ok():
    """
    Test if root returns correct welcome message
    """
    client = TestClient(app)
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == {"greeting": "Hello visitor!"}


def test_get_stores():
    """
    Test to assert if endpoint returns correct status code and data
    """
    app.db = db_mock([
        ['Djurjouren', 'Stockholm', 'Upplandsgatan 99', '12345'],
        ['Djuristen', 'Falun', 'Skånegatan 420', '54321'],
        ['Den Lilla Djurbutiken', 'Hudiksvall', 'Nätverksgatan 22', '55555'],
        ['Den Stora Djurbutiken', 'Hudiksvall', 'Routergatan 443', '54545'],
        ['Noahs Djur & Båtaffär', 'Gävle', 'Stallmansgatan 666', '96427']
    ])
    client = TestClient(app)
    response = client.get("/stores")
    assert response.status_code == 200
    assert response.json() == {
        "data": [
            {
                "name": "Djurjouren",
                "address": "Upplandsgatan 99, 12345 Stockholm"
            },
            {
                "name": "Djuristen",
                "address": "Skånegatan 420, 54321 Falun"
            },
            {
                "name": "Den Lilla Djurbutiken",
                "address": "Nätverksgatan 22, 55555 Hudiksvall"
            },
            {
                "name": "Den Stora Djurbutiken",
                "address": "Routergatan 443, 54545 Hudiksvall"
            },
            {
                "name": "Noahs Djur & Båtaffär",
                "address": "Stallmansgatan 666, 96427 Gävle"
            }
        ]
    }


def test_stores_by_name():
    """
    Test to assert if a correct store name returns correct status code and data
    """
    app.db = db_mock([('Djuristen', 'Falun', 'Skånegatan 420', '54321')])
    client = TestClient(app)
    response = client.get("/stores/Djuristen")
    assert response.status_code == 200
    assert response.json() == {
        "data": [
            {
                "name": "Djuristen",
                "address": "Skånegatan 420, 54321 Falun"
            }
        ]
    }


def test_stores_by_name_format_name():
    """
    Test to assert if a correct store name returns correct status code and data
    """
    app.db = db_mock([('Djuristen', 'Falun', 'Skånegatan 420', '54321')])
    client = TestClient(app)
    response = client.get("/stores/Djuristen?res_format=name")
    assert response.status_code == 200
    assert response.json() == {
        "data": [
            {
                "name": "Djuristen"
            }
        ]
    }


def test_stores_by_name_format_compact():
    """
    Test to assert if a correct store name returns correct status code and data
    """
    app.db = db_mock([('Djuristen', 'Falun', 'Skånegatan 420', '54321')])
    client = TestClient(app)
    response = client.get("/stores/Djuristen?res_format=compact")
    assert response.status_code == 200
    assert response.json() == {
        "data": [
            {
                "name": "Djuristen",
                "city": "Falun"
            }
        ]
    }


def test_stores_by_name_format_error():
    """
    Test to assert if a correct store name returns correct status code and data
    """
    app.db = db_mock([('Djuristen', 'Falun', 'Skånegatan 420', '54321')])
    client = TestClient(app)
    response = client.get("/stores/Djuristen?res_format=fel")
    assert response.status_code == 422
    assert response.json() == {
        'detail': [
            {
                'ctx': {'enum_values': ['full', 'name', 'compact']},
                'loc': ['query', 'res_format'],
                'msg':
                    "value is not a valid enumeration member; permitted: 'full', 'name', 'compact'",
                'type': 'type_error.enum'
            }
        ]
    }


def test_stores_by_name_bad():
    """
    Test to assert if wrong store name returns correct status code and data
    """
    app.db = db_mock([])
    client = TestClient(app)
    response = client.get("/stores/nonexistingstore")
    assert response.status_code == 404
    assert response.json() == {"detail": "404 Not Found"}


def test_cities():
    """
    Test to assert if endpoint returns correct status code and data
    """
    app.db = db_mock([('Falun',), ('Gävle',), ('Stockholm',), ('Hudiksvall',)])
    client = TestClient(app)
    response = client.get("/cities")
    assert response.status_code == 200
    assert response.json() == {
        "data": [
            "Falun",
            "Gävle",
            "Stockholm",
            "Hudiksvall"
        ]
    }


def test_cities_zip():
    """
    Test to assert if a correct zip code returns correct status code and data
    """
    app.db = db_mock([('Stockholm',)])
    client = TestClient(app)
    response = client.get("/cities?zipcode=12345")
    assert response.status_code == 200
    assert response.json() == {
        "data": [
            "Stockholm"
        ]
    }


def test_cities_multiple_zip():
    """
    Test to assert if multiple correct zip codes returns correct status code and data
    """
    app.db = db_mock([('Stockholm',), ('Falun',)])
    client = TestClient(app)
    response = client.get("/cities?zipcode=12345&zipcode=54321")
    assert response.status_code == 200
    assert response.json() == {
        "data": [
            "Stockholm",
            "Falun"
        ]
    }


def test_cities_zip_bad():
    """
    Test to assert if wrong zip code returns correct status code and data
    """
    app.db = db_mock([])
    client = TestClient(app)
    response = client.get("/cities?zipcode=99999")
    assert response.status_code == 404
    assert response.json() == {"detail": "404 Not Found"}


def test_sales():
    """
    Test to assert if return from /sales is correct
    """
    app.db = db_mock([
        ('Den Stora Djurbutiken',
         datetime.datetime(2022, 1, 25, 13, 52, 34),
         UUID('0188146f-5360-408b-a7c5-3414077ceb59')),
        ('Djuristen',
         datetime.datetime(2022, 1, 26, 15, 24, 45),
         UUID('726ac398-209d-49df-ab6a-682b7af8abfb')),
        ('Den Lilla Djurbutiken',
         datetime.datetime(2022, 2, 7, 9, 0, 56),
         UUID('602fbf9d-2b4a-4de2-b108-3be3afa372ae')),
        ('Den Stora Djurbutiken',
         datetime.datetime(2022, 2, 27, 12, 32, 46),
         UUID('51071ca1-0179-4e67-8258-89e34b205a1e'))])
    client = TestClient(app)
    response = client.get("/sales")
    assert response.status_code == 200
    assert response.json() == {
        "data": [
            {
                "store": "Den Stora Djurbutiken",
                "timestamp": "20220125T13:52:34",
                "sale_id": "0188146f-5360-408b-a7c5-3414077ceb59"
            },
            {
                "store": "Djuristen",
                "timestamp": "20220126T15:24:45",
                "sale_id": "726ac398-209d-49df-ab6a-682b7af8abfb"
            },
            {
                "store": "Den Lilla Djurbutiken",
                "timestamp": "20220207T09:00:56",
                "sale_id": "602fbf9d-2b4a-4de2-b108-3be3afa372ae"
            },
            {
                "store": "Den Stora Djurbutiken",
                "timestamp": "20220227T12:32:46",
                "sale_id": "51071ca1-0179-4e67-8258-89e34b205a1e"
            }
        ]
    }


def test_sales_id_by_valid_id():
    """
    Tests response from valid id that's also present in database
    """
    app.db = db_mock([
        (
            'Den Stora Djurbutiken',
            datetime.datetime(2022, 1, 25, 13, 52, 34),
            UUID('0188146f-5360-408b-a7c5-3414077ceb59'),
            'Hundmat',
            3
        ),
        (
            'Den Stora Djurbutiken',
            datetime.datetime(2022, 1, 25, 13, 52, 34),
            UUID('0188146f-5360-408b-a7c5-3414077ceb59'),
            'Sömnpiller och energidryck för djur',
            12
        )
    ])
    client = TestClient(app)
    response = client.get("/sales/0188146f-5360-408b-a7c5-3414077ceb59")
    assert response.status_code == 200
    assert response.json() == {
        "data": {
            "store": "Den Stora Djurbutiken",
            "timestamp": "20220125T13:52:34",
            "sale_id": "0188146f-5360-408b-a7c5-3414077ceb59",
            "products": [
                {
                    "name": "Hundmat",
                    "qty": 3
                },
                {
                    "name": "Sömnpiller och energidryck för djur",
                    "qty": 12
                }
            ]
        }
    }


def test_sales_id_not_found():
    """
    Tests uuid not found in database
    """
    app.db = db_mock([])
    client = TestClient(app)
    response = client.get("/sales/19e67404-6e35-45b7-8d6f-e5bc5b79c453")
    assert response.status_code == 404
    assert response.json() == {"detail": "404 Not Found"}


def test_sales_id_by_invalid_id():
    """
    Tests uuid by invalid format.
    Error message is raised from Pydantic.
    """
    app.db = db_mock("invalid-uuid-format")
    client = TestClient(app)
    response = client.get("/sales/invalid-uuid-format")
    assert response.status_code == 422
    assert response.json() == {
        "detail": [
            {
                "loc": ["path", "sale_id"],
                "msg": "value is not a valid uuid",
                "type": "type_error.uuid"
            }
        ]
    }


def test_get_income():
    """This unit test checks a call to GET /income without any query
    parameters.
    """
    app.db = db_mock(all_incomes)
    client = TestClient(app)
    response = client.get("/income")
    assert response.status_code == 200
    assert response.json() == return_data_income


def test_get_income_store():
    """This unit test checks a call to GET /income?store=UUID
    """
    data = [
        [
            "Den Lilla Djurbutiken",
            "Kattmat",
            109,
            57,
            "2022-02-07T09:00:56",
            "null"
        ]
    ]
    app.db = db_mock(data)
    client = TestClient(app)
    response = client.get("/income",
                          params={
                              "store": "ff53d831-c2fe-4fe8-9f67-5d69118670f2"})
    assert response.status_code == 200
    assert response.json() == {"data": [
        {
            "store_name": "Den Lilla Djurbutiken",
            "product_name": "Kattmat",
            "price": 109,
            "quantity": 57,
            "sale_time": "2022-02-07T09:00:56",
            "discount": "null"
        }
      ]
    }


def test_get_income_store_value_error():
    """This unit test checks a call to GET /income?store=UUID with an invalid UUID
    """
    app.db = db_mock("invalid_uuid")
    client = TestClient(app)
    response = client.get("/income",
                          params={"store": "bajs"})
    assert response.status_code == 422
    assert response.json() == {"detail": "Invalid UUID given for store!"}


def test_get_income_product():
    """This unit test checks a call to GET /income?store=UUID
    """
    data = [
        [
            "Den Stora Djurbutiken",
            "Hundmat",
            109,
            3,
            "2022-01-25T13:52:34",
            "null"
        ]
    ]
    app.db = db_mock(data)
    client = TestClient(app)
    response = client.get("/income",
                          params={
                              "product": "a37c34ae-0895-484a-8b2a-355aea3b6c44"})
    assert response.status_code == 200
    assert response.json() == {"data": [
        {
            "store_name": "Den Stora Djurbutiken",
            "product_name": "Hundmat",
            "price": 109,
            "quantity": 3,
            "sale_time": "2022-01-25T13:52:34",
            "discount": "null"
        }
      ]
    }


def test_get_income_product_value_error():
    """This unit test checks a call to GET /income?store=UUID with an invalid UUID
    """
    app.db = db_mock("invalid_uuid")
    client = TestClient(app)
    response = client.get("/income",
                          params={"product": "bajs"})
    assert response.status_code == 422
    assert response.json() == {"detail": "Invalid UUID given for product!"}


def test_get_income_store_product_from_to():
    """This unit test checks a call to GET /income with query parameters
    product and to
    """
    data = [
        [
            "Den Stora Djurbutiken",
            "Hundmat",
            109,
            3,
            "2022-01-25T13:52:34",
            "null"
        ]
    ]
    app.db = db_mock(data)
    client = TestClient(app)
    response = client.get("/income", params={
        "store": "676df1a1-f1d1-4ac5-9ee3-c58dfe820927",
        "product": "a37c34ae-0895-484a-8b2a-355aea3b6c44",
        "from": "2022-01-25",
        "to": "2022-01-26"
    })
    assert response.status_code == 200
    assert response.json() == {"data": [
            {
                "store_name": "Den Stora Djurbutiken",
                "product_name": "Hundmat",
                "price": 109,
                "quantity": 3,
                "sale_time": "2022-01-25T13:52:34",
                "discount": "null"
            }
        ]
    }


def test_get_inventory():
    """This unit test checks a call to GET /inventory without any query
    parameters.
    """
    app.db = db_mock(all_inventories)
    client = TestClient(app)
    response = client.get("/inventory")
    assert response.status_code == 200
    assert response.json() == return_data


def test_get_inventory_store():
    """This unit test checks a call to GET /inventory?store=UUID
    """
    data = list(filter(lambda x: x[-1] == "Den Stora Djurbutiken",
                       all_inventories))
    app.db = db_mock(data)
    client = TestClient(app)
    response = client.get("/inventory",
                          params={
                              "store": "676df1a1-f1d1-4ac5-9ee3-c58dfe820927"})
    assert response.status_code == 200
    assert response.json() == list(filter(
        lambda x: x["store_name"] == "Den Stora Djurbutiken", return_data))


def test_get_inventory_product():
    """This unit test checks a call to GET /inventory?product=UUID
    """
    data = list(filter(lambda x: x[0] == "Hundmat", all_inventories))
    app.db = db_mock(data)
    client = TestClient(app)
    response = client.get("/inventory",
                          params={
                              "product": "a37c34ae-0895-484a-8b2a-355aea3b6c44"
                          })
    assert response.status_code == 200
    assert response.json() == list(filter(
        lambda x: x["product_name"] == "Hundmat", return_data))


def test_get_inventory_store_and_product():
    """This unit test checks a call to GET /inventory?store=UUID&product=UUID
    """
    data = list(filter(
        lambda x: x[0] == "Hundmat" and x[-1] == "Den Stora Djurbutiken",
        all_inventories))
    app.db = db_mock(data)
    client = TestClient(app)
    response = client.get("/inventory", params={
        "product": "a37c34ae-0895-484a-8b2a-355aea3b6c44",
        "store": "676df1a1-f1d1-4ac5-9ee3-c58dfe820927"
    })
    assert response.status_code == 200
    assert response.json() == list(
        filter(
            lambda x:
            x["store_name"] == "Den Stora Djurbutiken" and
            x["product_name"] == "Hundmat", return_data))


def test_get_inventory_erroneous_store():
    """This unit test checks for a call to GET /inventory?store=Erroneous-UUID
    """
    app.db = db_mock(None)
    client = TestClient(app)
    response = client.get("/inventory",
                          params={"store": "this is not a valid UUID!"})
    assert response.status_code == 422
    assert response.json() == {"detail": "Invalid UUID for store!"}


def test_get_inventory_erroneous_product():
    """This unit test checks for a call to GET /inventory?product=Erroneous-UUID
    """
    app.db = db_mock(None)
    client = TestClient(app)
    response = client.get("/inventory",
                          params={"product": "this is not a valid UUID!"})
    assert response.status_code == 422
    assert response.json() == {"detail": "Invalid UUID for product!"}
